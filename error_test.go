package errors_test

import (
	errs "errors"
	"fmt"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/knopkalab/go/errors"
)

func TestError(t *testing.T) {
	customErr := errs.New("123")
	err := errors.Stack(customErr)
	err = errors.Stack(err)

	//var buf bytes.Buffer
	//err.WriteTo(&buf)
	//fmt.Println(buf.String())

	stack := err.StackLines()
	fmt.Println(stack)
	assert.Equal(t, "error_test.go:16", filepath.Base(stack[0]))
}
