package errors

import (
	"fmt"
	"io"
	"runtime"
)

// Error with stack trace
type Error interface {
	error
	Is(error) bool
	Unwrap() error

	io.WriterTo

	Skip(n int) Error
	Stack() *runtime.Frames
	StackLines() []string
}

// New error message with stack trace
func New(message string) Error {
	stack, n := getStack()
	return &withMessage{stack: stack, count: n, msg: message}
}

// Newf error formatted message with stack trace
func Newf(format string, args ...interface{}) Error {
	stack, n := getStack()
	return &withMessage{stack: stack, count: n, msg: fmt.Sprintf(format, args...)}
}

// Is Error check
func Is(err error) bool {
	_, ok := err.(Error)
	return ok
}

// Stack trace error wrap
func Stack(err error) Error {
	if err == nil {
		return nil
	}
	if err, ok := err.(Error); ok {
		return err
	}
	stack, n := getStack()
	return &withStack{error: err, stack: stack, count: n}
}
